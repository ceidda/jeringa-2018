package uy.edu.cei.jeringa.models;

import uy.edu.cei.jeringa.Jeringa;

public class TestModel {

	@Jeringa("test")
	private TestModelInjectable testModelInjectable;
	
	public TestModelInjectable getTestModelInjectable() {
		return this.testModelInjectable;
	}
}
